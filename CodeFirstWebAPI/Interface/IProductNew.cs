﻿using CodeFirstWebAPI.Models;
using System.Collections.Generic;
using System.Drawing;

namespace CodeFirstWebAPI.Interface
{
    public interface IProductNew
    {
        IEnumerable<ProductNewDetails> GetAllProduct();
        ProductNewDetails GetAllProductById(int id);
        void CreateProduct(ProductNewDetails product);
        //string CreateProduct(ProductNewDetails product);
        string Update(int id, ProductNewDetails product);
        string Reomve(int id);

        Image Convert_Base64To_Image(string base64string);
        void ImageUpload(string data, string fullFilePath);
    }
}
