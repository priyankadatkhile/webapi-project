﻿using System.Data.SqlClient;

namespace CodeFirstWebAPI.Interface
{
    public interface ISqlHelper
    {
        SqlConnection connect();
    }
}
