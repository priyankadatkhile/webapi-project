﻿using CodeFirstWebAPI.Models;
using System.Collections.Generic;

namespace CodeFirstWebAPI.Interface
{
    public interface IUser
    {
        IEnumerable<UserDetails> GetAllUser();
        UserDetails GetAllUserById(int id);
        void CreateUser(UserDetails user);
        string Update(int id, UserDetails user);
        string Reomve(int id);
    }
}
