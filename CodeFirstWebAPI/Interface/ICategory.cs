﻿using CodeFirstWebAPI.Models;
using System.Collections.Generic;

namespace CodeFirstWebAPI.Interface
{
    public interface ICategory
    {
        IEnumerable<CategoryDetails> GetAllCategory();
        CategoryDetails GetAllCategoryById(int id);
        void CreateCategory(CategoryDetails category);
        string Update(int id,Category category);
        string Reomve(int id);
    }
}
