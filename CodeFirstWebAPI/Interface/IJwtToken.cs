﻿using CodeFirstWebAPI.Models;

namespace CodeFirstWebAPI.Interface
{
    public interface IJwtToken
    {
        UserDetails ValidUser(string username, string password);
    }
}