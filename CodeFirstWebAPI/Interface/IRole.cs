﻿using CodeFirstWebAPI.Models;
using System.Collections.Generic;

namespace CodeFirstWebAPI.Interface
{
    public interface IRole
    {
        IEnumerable<RoleDetails> GetAllRole();
        RoleDetails GetAllRoleById(int id);
        void CreateRole(RoleDetails role);
        string Update(int id, RoleDetails role);
        string Reomve(int id);
    }
}
