﻿namespace CodeFirstWebAPI.Interface
{
    interface ISolidSRPUser
    {
        bool Login(string username, string password);
        bool Registration(string username, string password,string email);
    }
    interface ILogger
    {
        void LogError(string error);
    }

    interface IEmail
    {
        bool SendEmail(string emailContent);
    }
}
