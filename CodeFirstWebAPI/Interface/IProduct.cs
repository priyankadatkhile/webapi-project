﻿using CodeFirstWebAPI.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace CodeFirstWebAPI.Interface
{
    public interface IProduct
    {
        IEnumerable<CategoryDetails> GetAllCategory();
        string Create1();
        HttpResponseMessage SaveImage(string ImgName, string ImgStr);
    }
}
