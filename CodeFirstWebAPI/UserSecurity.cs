﻿using CodeFirstWebAPI.Models;
using System;
using System.Linq;

namespace CodeFirstWebAPI
{
    public class UserSecurity
    {
        public static bool Login(string username, string password)
        {
            using (WebApiContext db = new WebApiContext())
            {
                return db.UserDetails.Any(user => user.Username.Equals(username,StringComparison.OrdinalIgnoreCase) && user.Password == password);    // Any()- used to check whether at least one of the elements of a data source satisfies the given condition or not. 
                // StringComparison.OrdinalIgnoreCase- used for case sensitivity
            }
        }
    }
}