﻿using Autofac;
using Autofac.Integration.WebApi;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Services;
using System.Reflection;
using System.Web.Http;

namespace CodeFirstWebAPI.App_Start
{
    public static class AutofacConfig
    {
        public static void Configuration()
        {
            var builder = new ContainerBuilder();
            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;
            // Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            // OPTIONAL: Register the Autofac filter provider.
            builder.RegisterWebApiFilterProvider(config);
            // This will register the required service class object in above snippets.
            builder.RegisterType<CategoryServices>().As<ICategory>();
            builder.RegisterType<ProductNewServices>().As<IProductNew>();
            builder.RegisterType<RoleServices>().As<IRole>();
            builder.RegisterType<UserServices>().As<IUser>();
            builder.RegisterType<JwtTokensServices>().As<IJwtToken>();
            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}