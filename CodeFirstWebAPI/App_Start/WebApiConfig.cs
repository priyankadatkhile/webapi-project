﻿using CodeFirstWebAPI.Controllers;
using CodeFirstWebAPI.Filters;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CodeFirstWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                //routeTemplate: "api/{controller}/{action}/{id}",
                //routeTemplate: "{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.Filters.Add(new MyActionFilter2("filter at global lavel"));

            config.Filters.Add(new MyExceptionFilter2());
        }
    }
}
