﻿using System.Collections.Generic;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Linq;
using System;
using System.Data.Entity;

namespace CodeFirstWebAPI.Services
{
    public class UserServices : IUser
    {
        WebApiContext db = new WebApiContext();
        public void CreateUser(UserDetails user)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    ctx.UserDetails.Add(new UserDetails
                    {
                        UserId = user.UserId,
                        RoleName = user.RoleName,
                        Username = user.Username,
                        Password = user.Password,
                        Email = user.Email
                    });
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public IEnumerable<UserDetails> GetAllUser()
        {
            return db.UserDetails.ToList();
        }

        public UserDetails GetAllUserById(int id)
        {
            UserDetails user = db.UserDetails.FirstOrDefault(u => u.UserId == id);
            return user;
        }

        public string Reomve(int id)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingUser = ctx.UserDetails.Where(u => u.UserId == id).FirstOrDefault();

                    if (existingUser == null)
                    {
                        return "User with Id " + id.ToString() + " not found";
                    }
                    else
                    {
                        ctx.Entry(existingUser).State = EntityState.Deleted;
                        ctx.SaveChanges();
                        return "User with Id " + id.ToString() + " Deleted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(int id, UserDetails user)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingUser = ctx.UserDetails.Where(u => u.UserId == id).FirstOrDefault();

                    if (existingUser != null)
                    {
                        existingUser.Username = user.Username;
                        existingUser.RoleName = user.RoleName;
                        existingUser.Password = user.Password;
                        existingUser.Email = user.Email;

                        ctx.SaveChanges();
                        return "User with Id " + id.ToString() + " Updated Successfully";
                    }
                    else
                    {
                        return "User with Id " + id.ToString() + " not found";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}