﻿using System.Collections.Generic;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Linq;
using System;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Data.Entity;

namespace CodeFirstWebAPI.Services
{
    public class ProductNewServices : IProductNew
    {
        WebApiContext db = new WebApiContext();

        public Image Convert_Base64To_Image(string base64string)
        {
            // convert base64 to string to byte
            byte[] bytes = Convert.FromBase64String(base64string); 
            //Convert.FromBase64String("R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");

            Image image;

            // Convert byte[] to image
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return image;
        }

        public void ImageUpload(string data, string fullFilePath)
        {
            Image image = Convert_Base64To_Image(data);
            var bitmapImage = new Bitmap(image);
            bitmapImage.Save(fullFilePath, ImageFormat.Jpeg);
        }

        public void CreateProduct(ProductNewDetails product)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    ctx.ProductNewDetails.Add(new ProductNewDetails
                    {
                        ProductId = product.ProductId,
                        CategoryId = product.CategoryId,
                        ProductName = product.ProductName,
                        Description = product.Description,
                        ProductPrice = product.ProductPrice,
                        ProductImage = product.ProductImage
                    });
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string CreateProduct(ProductNewDetails product)
        //{

        //    Dictionary<string, object> dict = new Dictionary<string, object>();
        //    try
        //    {
        //        var httpRequest = HttpContext.Current.Request;

        //        foreach (string file in httpRequest.Files)
        //        {
        //            var filePath = "";
        //            HttpResponseMessage response = new HttpResponseMessage();

        //            var postedFile = httpRequest.Files[file];
        //            //var productName = httpRequest.Files[product.ProductName];

        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {

        //                int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  

        //                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedFileExtensions.Contains(extension))
        //                {

        //                    var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

        //                    dict.Add("error", message);
        //                    return dict.ToString();
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {

        //                    var message = string.Format("Please Upload a file upto 1 mb.");

        //                    dict.Add("error", message);
        //                    return dict.ToString();
        //                }
        //                else
        //                {
        //                    filePath = HttpContext.Current.Server.MapPath("~/Image/" + postedFile.FileName + extension);
        //                    postedFile.SaveAs(filePath);

        //                    product.ProductImage = filePath;
        //                    db.ProductNewDetails.Add(product);
        //                    db.SaveChanges();
        //                }
        //            }
        //            var message1 = string.Format("Image Updated Successfully.");
        //            dict.Add("success", message1);
        //            return dict.ToString();
        //        }
        //        var res = string.Format("Please Upload a image.");
        //        dict.Add("error", res);
        //        return dict.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        var res = string.Format("some Message");
        //        dict.Add("error", res);
        //        return dict.ToString();
        //    }
        //}
        
        public IEnumerable<ProductNewDetails> GetAllProduct()
        {
            return db.ProductNewDetails.ToList();
        }

        public ProductNewDetails GetAllProductById(int id)
        {
            var entity = db.ProductNewDetails.FirstOrDefault(p => p.ProductId == id);
            return entity;
        }

        public string Reomve(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return "Product with Id " + id.ToString() + " not found";
                }
                using (var ctx = new WebApiContext())
                {
                    var existingProduct = ctx.ProductDetails.Where(p => p.ProductId == id).FirstOrDefault();

                    ctx.Entry(existingProduct).State = EntityState.Deleted;
                    ctx.SaveChanges();
                    return "Product with Id " + id.ToString() + " Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(int id, ProductNewDetails product)
        {try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingProduct = ctx.ProductDetails.Where(p => p.ProductId == id).FirstOrDefault();

                    if (existingProduct != null)
                    {

                        existingProduct.CategoryId = product.CategoryId;
                        existingProduct.ProductName = product.ProductName;
                        existingProduct.Description = product.Description;
                        existingProduct.ProductPrice = product.ProductPrice;
                        existingProduct.ProductImage = product.ProductImage;

                        ctx.SaveChanges();
                        return "Product with Id " + id.ToString() + " Updated Successfully";
                    }
                    else
                    {
                        return "Product with Id " + id.ToString() + " not found";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}