﻿using System.Collections.Generic;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Linq;
using System.Data.Entity;
using System;

namespace CodeFirstWebAPI.Services
{
    public class CategoryServices : ICategory
    {
        WebApiContext db = new WebApiContext();
        public void CreateCategory(CategoryDetails category)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    ctx.CategoryDetails.Add(new CategoryDetails
                    {
                        CategoryId = category.CategoryId,
                        CategoryName = category.CategoryName
                    });
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<CategoryDetails> GetAllCategory()
        {
            return db.CategoryDetails.ToList();
        }

        public CategoryDetails GetAllCategoryById(int id)
        {
            CategoryDetails categories = db.CategoryDetails.AsNoTracking().Where(c => c.CategoryId == id).FirstOrDefault();
            return categories;
        }

        public string Reomve(int id)
        {
            try
            {
                //if (id <= 0)
                //{
                //    return "Category with Id " + id.ToString() + " not found";
                //}
                using (var ctx = new WebApiContext())
                {
                    var existingCategory = ctx.CategoryDetails.Where(c => c.CategoryId == id).FirstOrDefault();

                    if (existingCategory == null)
                    {
                        return "Category with Id " + id.ToString() + " not found";
                    }
                    else
                    {
                        ctx.Entry(existingCategory).State = EntityState.Deleted;
                        ctx.SaveChanges();
                        return "Category with Id " + id.ToString() + " Deleted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(int id, Category category)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingCategory = ctx.CategoryDetails.Where(c => c.CategoryId == id).FirstOrDefault();

                    if (existingCategory != null)
                    {
                        existingCategory.CategoryName = category.CategoryName;

                        ctx.SaveChanges();
                        return "Category with Id " + id.ToString() + " Updated Successfully";
                    }
                    else
                    {
                        return "Category with Id " + id.ToString() + " not found";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}