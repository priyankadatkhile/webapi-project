﻿using System.Collections.Generic;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Linq;
using System;
using System.Data.Entity;

namespace CodeFirstWebAPI.Services
{
    public class RoleServices : IRole
    {
        WebApiContext db = new WebApiContext();

        public void CreateRole(RoleDetails role)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    ctx.RoleDetails.Add(new RoleDetails
                    {
                        RoleId = role.RoleId,
                        RoleName = role.RoleName
                    });
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<RoleDetails> GetAllRole()
        {
            return db.RoleDetails.ToList();
        }

        public RoleDetails GetAllRoleById(int id)
        {
            RoleDetails entity = db.RoleDetails.FirstOrDefault(r => r.RoleId == id);
            return entity;
        }

        public string Reomve(int id)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingRole = ctx.RoleDetails.Where(r => r.RoleId == id).FirstOrDefault();

                    if (existingRole == null)
                    {
                        return "Role with Id " + id.ToString() + " not found";
                    }
                    else
                    {
                        ctx.Entry(existingRole).State = EntityState.Deleted;
                        ctx.SaveChanges();
                        return "Role with Id " + id.ToString() + " Deleted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Update(int id, RoleDetails role)
        {
            try
            {
                using (var ctx = new WebApiContext())
                {
                    var existingRole = ctx.RoleDetails.Where(r => r.RoleId == id).FirstOrDefault();

                    if (existingRole != null)
                    {
                        existingRole.RoleName = role.RoleName;

                        ctx.SaveChanges();
                        return "Role with Id " + id.ToString() + " Updated Successfully";
                    }
                    else
                    {
                        return "Role with Id " + id.ToString() + " not found";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}