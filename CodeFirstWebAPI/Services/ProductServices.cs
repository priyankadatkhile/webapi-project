﻿using System;
using System.Collections.Generic;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Net;
using System.IO;

namespace CodeFirstWebAPI.Services
{
    public class ProductServices : IProduct
    {
        WebApiContext db = new WebApiContext();

        public string Create1()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            ProductDetails product = new ProductDetails();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    var filePath = "";
                    HttpResponseMessage response = new HttpResponseMessage();

                    var postedFile = httpRequest.Files[file];
                    var productName = httpRequest.Files[product.ProductName];

                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return dict.ToString();
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return dict.ToString();
                        }
                        else
                        {
                            filePath = HttpContext.Current.Server.MapPath("~/Image/" + postedFile.FileName + extension);
                            postedFile.SaveAs(filePath);

                        }


                        product.ProductImage = filePath;
                        db.ProductDetails.Add(product);
                        db.SaveChanges();
                    }


                    var message1 = string.Format("Image Updated Successfully.");
                    return dict.ToString();
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return dict.ToString();
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message");
                dict.Add("error", res);
                return dict.ToString();
            }
        }

        public IEnumerable<CategoryDetails> GetAllCategory()
        {
            List<CategoryDetails> category = db.CategoryDetails.OrderByDescending(x => x.CategoryName).ToList();
            return category;
        }

        public HttpResponseMessage SaveImage(string ImgName, string ImgStr)
        {

            string path = HttpContext.Current.Server.MapPath("~/Image"); //Path

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = ImgName + ".jpg";

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(ImgStr);

            File.WriteAllBytes(imgPath, imageBytes);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        //public void GetCategory()
        //{
        //    IList<CategoryDetails> catObj = db.CategoryDetails.Include("CategoryDetails").Select(x => new CategoryDetails()
        //    {
        //        CategoryId = x.CategoryId,
        //        CategoryName = x.CategoryName
        //    }).ToList();
        //}

    }
}