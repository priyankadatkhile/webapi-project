﻿using CodeFirstWebAPI.Interface;
using System;
using System.Linq;
using CodeFirstWebAPI.Models;

namespace CodeFirstWebAPI.Services
{
    public class JwtTokensServices : IJwtToken
    {
        WebApiContext db = new WebApiContext();

        public UserDetails ValidUser(string username, string password)
        {
            return db.UserDetails.FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase) && user.Password == password);
        }
    }
}