﻿using CodeFirstWebAPI.Models;
using System;
using System.Linq;

namespace CodeFirstWebAPI
{
    public class UserDetailsRepository : IDisposable
    {
        WebApiContext db = new WebApiContext();

        public UserDetails ValidateUser(string username,string password)
        {
            return db.UserDetails.FirstOrDefault(user => user.Username.Equals(username, StringComparison.OrdinalIgnoreCase) && user.Password == password);
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}