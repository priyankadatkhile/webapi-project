﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class ExceptionLog
    {
        [Key]
        public int id { get; set; }
        public DateTime timestamp { get; set; }
        public string level { get; set; }
        public string logger { get; set; }
        public string message { get; set; }
        public Nullable<int> userid { get; set; }
        public string exception { get; set; }
        public string stacktrace { get; set; }
    }
}