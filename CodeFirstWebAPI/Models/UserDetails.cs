﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class UserDetails
    {
        [Key]
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}