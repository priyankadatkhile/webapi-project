﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class ResponseHeaderModel
    {
        [Key]
        public int Id { get; set; }
        public string Buffer { get; set; }
        public string BufferOutput { get; set; }
        public string ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public string Expires { get; set; }
        public string Status { get; set; }
    }
}