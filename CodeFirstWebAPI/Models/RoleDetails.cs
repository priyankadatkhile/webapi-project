﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class RoleDetails
    {
        [Key]
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}