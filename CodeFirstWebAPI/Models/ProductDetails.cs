﻿using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class ProductDetails
    {
        [Key]
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductImage { get; set; }
        public string CategoryName { get; set; }
        public CategoryDetails Category { get; set; }
    }
}