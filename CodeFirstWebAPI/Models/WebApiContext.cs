﻿using System.Data.Entity;

namespace CodeFirstWebAPI.Models
{
    public class WebApiContext : DbContext
    {
        public WebApiContext() : base("ConnectionString")
        {

        }

        public DbSet<CategoryDetails> CategoryDetails { get; set; }
        public DbSet<ProductDetails> ProductDetails { get; set; }
        public DbSet<ProductNewDetails> ProductNewDetails { get; set; }
        public DbSet<RoleDetails> RoleDetails { get; set; }
        public DbSet<UserDetails> UserDetails { get; set; }
        //public DbSet<ResponseHeaderModel> ResponseHeaderModels { get; set; }
    }
}