﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstWebAPI.Models
{
    public class CategoryDetails
    {
        [Key]
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public virtual ICollection<ProductDetails> Products { get; set; }

    }
}