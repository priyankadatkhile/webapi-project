﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace CodeFirstWebAPI.Filters
{
    //option 1

    public class MyExceptionFilter : FilterAttribute, IExceptionFilter
    {
        public Task ExecuteExceptionFilterAsync(
            HttpActionExecutedContext context,
            CancellationToken cancellationToken)
        {
            Action action = () =>
            {
                throw new EndOfWorldException();
                if (context.Exception is EndOfWorldException)
                {
                    var actName = context.ActionContext.ActionDescriptor.ActionName;

                    var msg = $"MyExceptionFilter:action - {actName}";
                    Trace.WriteLine(msg);

                    context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, msg);
                }
            };

            var task = new Task(action);
            task.Start();

            return task;
        }
    }
    //option 2

    public class MyExceptionFilter2 : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is EndOfWorldException)
            {
                var actName = context.ActionContext.ActionDescriptor.ActionName;

                var msg = $"MyExceptionFilter2:action - {actName}";
                Trace.WriteLine(msg);

                context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, msg);
            }
        }
    }

    // just a simple exception without any implementation
    public class EndOfWorldException : Exception
    { }
}