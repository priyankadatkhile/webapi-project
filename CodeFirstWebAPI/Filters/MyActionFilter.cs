﻿using CodeFirstWebAPI.Controllers;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CodeFirstWebAPI.Filters
{
    // option implement IActionFilter interface
    public class MyActionFilter : Attribute, IActionFilter
    {
        public string id { get; set; }

        public MyActionFilter(string n)
        {
            id = n;
        }
        public bool AllowMultiple => true;

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(
            HttpActionContext actionContext,
            CancellationToken cancellationToken,
            Func<Task<HttpResponseMessage>> continuation)
        {
            var actName =
                actionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{id} Before execution of {actName}");

            var result = continuation();
            result.Wait();

            Trace.WriteLine($"{id} After execution of {actName}");

            return result;
        }
    }

    // option extend ActionFilterAttribute class

    public class MyActionFilter2 : ActionFilterAttribute
    {
        public string id { get; set; }

        public MyActionFilter2(string n)
        {
            id = n;
        }
        //before
        public override void OnActionExecuting(
            HttpActionContext actionContext)
        {
            //throw new EndOfWorldException();

            var actName = actionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{id} Before execution of {actName}");
        }

        //after
        public override void OnActionExecuted(
            HttpActionExecutedContext actionExecutedContext)
        {
            var actName = actionExecutedContext.ActionContext.ActionDescriptor.ActionName;

            Trace.WriteLine($"{id} After execution of {actName}");
        }
    }
}