﻿using CodeFirstWebAPI.Models;
using CodeFirstWebAPI.Services;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace CodeFirstWebAPI.Filters
{
    public class AddHeaderRequest : System.Web.Http.Filters.ActionFilterAttribute
    {
        SqlHelper conn = new SqlHelper();

        void OnActionExecuting(
            ActionExecutingContext filterContext)
        {
            ResponseHeaderModel model = new ResponseHeaderModel();
            var header = filterContext.RequestContext.HttpContext.Response.Headers;
            var Response = filterContext.RequestContext.HttpContext.Response;
            model.Buffer = Response.Buffer.ToString();
            model.BufferOutput = Response.BufferOutput.ToString();
            model.ContentEncoding = Response.ContentEncoding.ToString();
            model.ContentType = Response.ContentType;
            model.Expires = Response.Expires.ToString();
            model.Status = Response.Status;

            var con = conn.connect();
            con.Open();
            string sql = "insert into ResponseHeaderModel (Id,Buffer,BufferOutput,ContentEncoding,ContentType,Expires,Status) values(@Id,@Buffer,@BufferOutput,@ContentEncoding,@ContentType,@Expires,@Status)";

            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.AddWithValue("@Id", 0);
            cmd.Parameters.AddWithValue("@Buffer", model.Buffer);
            cmd.Parameters.AddWithValue("@BufferOutput", model.BufferOutput);
            cmd.Parameters.AddWithValue("@ContentEncoding", model.ContentEncoding);
            cmd.Parameters.AddWithValue("@ContentType", model.ContentType);
            cmd.Parameters.AddWithValue("@Expires", model.Expires);
            cmd.Parameters.AddWithValue("@Status", model.Status);
            cmd.ExecuteNonQuery();
        }
    }
}