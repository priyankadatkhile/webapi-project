﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CodeFirstWebAPI
{
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            // if header is missing
            if(actionContext.Request.Headers.Authorization==null)       // if it is null it means client not send credentials
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            else
            {
                // if header is there
                string authenticationToken = actionContext.Request.Headers.Authorization.Parameter;
                // To decoded base64
                string decodedAuthenticationToken = Encoding.UTF8.GetString(Convert.FromBase64String(authenticationToken));
                // split username and password to (username:password) this format
                string[] usernamePasswordArray = decodedAuthenticationToken.Split(':');
                string username = usernamePasswordArray[0];
                string password = usernamePasswordArray[1];

                string[] roles = { "User", "Admin" };

                if (UserSecurity.Login(username, password))
                {
                    //Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username), null);
                    Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username), roles);
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
        }
    }
}