﻿using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class RoleController : ApiController
    {
        private readonly IRole _role;

        public RoleController(IRole role)
        {
            _role = role;
        }

        public HttpResponseMessage GetRole()
        {
            var roles = _role.GetAllRole();
            return Request.CreateResponse(HttpStatusCode.OK, roles);
        }

        [HttpGet]
        public HttpResponseMessage GetRoleById(int id)
        {
            RoleDetails roles = _role.GetAllRoleById(id);
            if (roles != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, roles);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Role with Id = " + id.ToString() + " not found");
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateRole(RoleDetails role)
        {
            _role.CreateRole(role);
            return Request.CreateResponse(HttpStatusCode.Created, role);
        }

        [HttpPut]
        public HttpResponseMessage UpdateRole(int id, RoleDetails role)
        {
            var roles = _role.Update(id, role);
            return Request.CreateResponse(HttpStatusCode.Created, roles);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteRole(int id)
        {
            var roles = _role.Reomve(id);
            return Request.CreateResponse(HttpStatusCode.OK, roles);
        }
    }
}
