﻿using System.Web.Mvc;
using NLog;
using System;

namespace CodeFirstWebAPI.Controllers
{
    public class LogFilterController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: LogFilter
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            logger.Info("Hello You have visited the Index view" + Environment.NewLine + DateTime.Now);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            logger.Info("hello now you have visited the About view" + Environment.NewLine + DateTime.Now);
            return View();
        }
    }
}