﻿using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IJwtToken _token;
        public AccountController(IJwtToken token)
        {
            _token = token;
        }
        
        [HttpGet]
        public HttpResponseMessage ValidLogin(string userName, string userPassword)
        {
            try
            {
                UserDetails obj = _token.ValidUser(userName, userPassword);
                
                if(userName == obj.Username && userPassword == obj.Password)
                    return Request.CreateResponse(HttpStatusCode.OK, TokenManager.GenerateToken(userName));
                else
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid");

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid UserName & Password");
            }
        }

        [HttpGet]
        [CustomAuthenticationFilter]
        public HttpResponseMessage GetUser()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Successfully Valid");
        }
    }
}
