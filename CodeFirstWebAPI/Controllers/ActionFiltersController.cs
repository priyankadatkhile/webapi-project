﻿using CodeFirstWebAPI.Filters;
using System.Diagnostics;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    [MyActionFilter("filter at controller level")]
    public class ActionFiltersController : ApiController
    {
        [MyActionFilter("filter 1")]
        [Route("api/af/get")]
        public string Get()
        {
            Trace.WriteLine("Inside action method get...");
            return "Action Get from Action Filters Controller";
        }

        [MyActionFilter2("filter 2")]
        [Route("api/af/get2")]
        public string Get2()
        {
            Trace.WriteLine("Inside action method get2...");
            return "Action Get2 from Action Filters Controller";
        }
    }
}
