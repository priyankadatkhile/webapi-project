﻿using CodeFirstWebAPI.Filters;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class ExceptionFiltersController : ApiController
    {
        [MyExceptionFilter]
        [Route("api/ef/get")]
        public string Get()
        {
            throw new EndOfWorldException();
        }

        [MyExceptionFilter2]
        [Route("api/ef/get2")]
        public string Get2()
        {
            throw new EndOfWorldException();
        }

        // normal method
        [Route("api/ef/getNoExceptions")]
        public string GetNoExceptions()
        {
            return "abc";
        }
    }
}
