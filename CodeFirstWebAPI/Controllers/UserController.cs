﻿using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUser _user;

        public UserController(IUser user)
        {
            _user = user;
        }

        public HttpResponseMessage GetUser()
        {
            var users = _user.GetAllUser();
            return Request.CreateResponse(HttpStatusCode.OK, users);
        }

        [HttpGet]
        public HttpResponseMessage GetUserById(int id)
        {
            UserDetails users = _user.GetAllUserById(id);
            if (users != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, users);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "User with Id = " + id.ToString() + " not found");
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateUser(UserDetails user)
        {
            _user.CreateUser(user);
            return Request.CreateResponse(HttpStatusCode.Created, user);
        }

        [HttpPut]
        public HttpResponseMessage UpdateUser(int id, UserDetails user)
        {
            var users = _user.Update(id, user);
            return Request.CreateResponse(HttpStatusCode.Created, users);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteUser(int id)
        {
            var users = _user.Reomve(id);
            return Request.CreateResponse(HttpStatusCode.OK, users);
        }
    }
}
