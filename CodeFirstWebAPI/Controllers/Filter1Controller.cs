﻿using CodeFirstWebAPI.Filters;
using CodeFirstWebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    [Log]
    public class Filter1Controller : ApiController
    {
        public IEnumerable<UserDetails> Get()
        {
            WebApiContext db = new WebApiContext();
            return db.UserDetails.ToList();
        }
    }
}
