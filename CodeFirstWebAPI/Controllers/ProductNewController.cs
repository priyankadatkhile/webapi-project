﻿using CodeFirstWebAPI.Filters;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class ProductNewController : ApiController
    {
        private readonly IProductNew _product;

        public ProductNewController(IProductNew product)
        {
            _product = product;
        }

        [CustomAuthenticationFilters]
        public HttpResponseMessage GetProduct()
        {
            var products = _product.GetAllProduct();
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }

        [HttpGet]
        public HttpResponseMessage GetProductById(int id)
        {
            ProductNewDetails products = _product.GetAllProductById(id);
            if(products != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, products);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Product with Id = " + id.ToString() + " not found");
            }
        }
        
        [HttpPost]
        public HttpResponseMessage CreateProduct(ProductNewDetails products)
        {
            _product.CreateProduct(products);
            return Request.CreateResponse(HttpStatusCode.Created, products);
        }

        [HttpPut]
        public HttpResponseMessage UpdateProduct(int id, ProductNewDetails product)
        {
            var products = _product.Update(id, product);
            return Request.CreateResponse(HttpStatusCode.Created, products);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteProduct(int id)
        {
            var products = _product.Reomve(id);
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }
    }
}
