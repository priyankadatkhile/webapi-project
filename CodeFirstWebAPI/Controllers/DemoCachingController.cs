﻿using CodeFirstWebAPI.Filters;
using CodeFirstWebAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class DemoCachingController : ApiController
    {
        private WebApiContext db = new WebApiContext();

        [HttpGet]
        [CachFilter(TimeDuration = 400)]
        public IEnumerable<CategoryDetails> GetCategory()
        {
            return db.CategoryDetails.ToList();
        }
    }
}
