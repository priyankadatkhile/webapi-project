﻿using CodeFirstWebAPI.Filters;
using CodeFirstWebAPI.Interface;
using CodeFirstWebAPI.Models;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    public class CategoryController : ApiController
    {
        private readonly ICategory _category;

        public CategoryController(ICategory category)
        {
            _category = category;
        }

        [BasicAuthentication]
        [CachFilter(TimeDuration = 400)]
        public HttpResponseMessage GetCategory()
        {
            string username = Thread.CurrentPrincipal.Identity.Name;    // Retrieving username 
            //return Request.CreateResponse(HttpStatusCode.OK, username);

            var category = _category.GetAllCategory();
            return Request.CreateResponse(HttpStatusCode.OK, category);
        }

        [HttpGet]
        public HttpResponseMessage GetCategoryById(int id)
        {
            CategoryDetails entity = _category.GetAllCategoryById(id);
            if (entity != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Category with Id = " + id.ToString() + " not found");
            }
        }

        [HttpPost]
        public HttpResponseMessage CreateCategory(CategoryDetails category)
        {
            _category.CreateCategory(category);
            return Request.CreateResponse(HttpStatusCode.Created, category);
        }

        [HttpPut]
        public HttpResponseMessage UpdateCategory(int id, Category category)
        {
            var categories = _category.Update(id, category);
            return Request.CreateResponse(HttpStatusCode.Created, categories);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteCategory(int id)
        {
            var categories = _category.Reomve(id);
            return Request.CreateResponse(HttpStatusCode.OK, categories);
        }
    }
}
