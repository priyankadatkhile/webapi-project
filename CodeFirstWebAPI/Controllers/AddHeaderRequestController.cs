﻿using CodeFirstWebAPI.Filters;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeFirstWebAPI.Controllers
{
    [AddHeaderRequest]
    public class AddHeaderRequestController : ApiController
    {
        public string Get()
        {
            return "Action Get from Action Filters Controller";
        }

        [HttpPost]
        public HttpResponseMessage Post()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
